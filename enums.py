from enum import Enum
from dateutil.rrule import MONTHLY, YEARLY, WEEKLY, DAILY


class FrequencyRule(Enum):
    Never = 0
    Once = 1
    Yearly = 2
    Quarterly = 3
    Monthly = 4
    BiWeekly = 5
    Weekly = 6
    Daily = 7
    Every = 8


FREQ_RRULE_MAP = {
    FrequencyRule.Never: YEARLY,
    FrequencyRule.Once: YEARLY,
    FrequencyRule.Yearly: YEARLY,
    FrequencyRule.Quarterly: MONTHLY,
    FrequencyRule.Monthly: MONTHLY,
    FrequencyRule.BiWeekly: WEEKLY,
    FrequencyRule.Weekly: WEEKLY,
    FrequencyRule.Daily: DAILY,
}


def get_rrule_from_freq_type(t: FrequencyRule) -> int:
    return FREQ_RRULE_MAP[FrequencyRule(t)]
