from argparse import ArgumentParser, Namespace
import yaml
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from typing import List

from models import Expense, IncomeProfile, IncomeSource, SavingsAccount, TaxBracket
from financial_pipeline import FinancialPipeline


def collect_args() -> Namespace:
    parser = ArgumentParser()
    parser.add_argument(
        "-f",
        "--file",
        help="Path to fiscal profile YAML configuration",
        default="sample.yml",
    )
    return parser.parse_args()


def main():
    args = collect_args()
    with open(args.file, "r") as f:
        profile = yaml.load(f, Loader=yaml.SafeLoader)

    expenses = [Expense(**expense) for expense in profile["expenses"]]
    income_sources = [IncomeSource(**data) for data in profile["income_sources"]]
    savings = [SavingsAccount(**data) for data in profile["accounts"]]

    income_profile = IncomeProfile(
        expenses=expenses, income_sources=income_sources, savings=savings
    )

    pipeline = FinancialPipeline(income_profile)
    tick_size = timedelta(days=1)

    start = datetime.now()
    time_change = relativedelta(years=10)

    pipeline.build(start, start + time_change)
    pipeline.execute(tick_size)

    print("FINAL:")
    income_profile.print_accounts_table()


if __name__ == "__main__":
    main()
