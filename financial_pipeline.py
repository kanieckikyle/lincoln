from models import IncomeProfile
from typing import List, Dict
from datetime import datetime, timedelta
import yaml
from os import walk

from models import TaxBracket

from operators import BaseOperator


class FinancialPipeline:

    initial_profile: IncomeProfile

    income_profile: IncomeProfile

    operators: List[BaseOperator]

    timestamp: datetime

    end: datetime

    tax_map: Dict[int, List[TaxBracket]]

    def __init__(
        self,
        profile: IncomeProfile,
        tax_fixture_dir: str = "tax_bracket_fixtures",
        tax_filing_status="single",
    ):
        self.initial_profile = profile.copy()
        self.income_profile = profile
        self.operators = []

        self.tax_map = {}
        self.tax_fixture_dir = tax_fixture_dir
        self.tax_filing_status = tax_filing_status

        self.timestamp = None
        self.end = None

    def build(self, start: datetime, end: datetime):
        self.timestamp = start
        self.end = end

        self.load_taxes()
        self.income_profile.set_taxes(self.get_taxes_for_year(self.timestamp.year))

        tickables = (
            self.income_profile.income_sources
            + self.income_profile.expenses
            + self.income_profile.savings
        )
        for tickable in tickables:
            tickable.build_ticks(start, end)

    def execute(self, tick_size: timedelta):
        assert (
            self.timestamp is not None and self.end is not None
        ), "You must build the pipeline before executing it!"

        while self.timestamp <= self.end:
            self.tick(tick_size)

    def tick(self, delta: timedelta = None):
        change = delta or timedelta(days=0)
        self.income_profile.tick(change)

        old = self.timestamp
        self.timestamp += change

        # If we are changing tax years, lets apply the new taxes to our profile
        if old.year < self.timestamp.year:
            print(f"\nIncome Profile after {old.year}")
            self.income_profile.print_accounts_table()

            print(f"\nApplying new taxes for {self.timestamp.year}")
            self.income_profile.set_taxes(self.get_taxes_for_year(self.timestamp.year))

    def get_taxes_for_year(self, search_year: int) -> List[TaxBracket]:
        # First, straight up try to get the year
        try:
            return self.tax_map[search_year]
        except KeyError:
            pass

        # Get the keys from the dictionary if we can't find it
        tax_years = list(self.tax_map.keys())

        # Check the edge conditions first for efficiency
        if search_year <= tax_years[0]:
            return self.tax_map[tax_years[0]]

        if search_year >= tax_years[-1]:
            return self.tax_map[tax_years[-1]]

        # Then do an actual search through the array
        closest_year = tax_years[0]
        for curr_year in tax_years[1 : len(tax_years) - 1]:
            if curr_year == search_year:
                return self.tax_map[curr_year]
            if curr_year > search_year:
                return closest_year
            closest_year = curr_year
        else:
            return self.tax_map[-1]

    def load_taxes(self):
        _, _, tax_fixtures = next(walk(self.tax_fixture_dir))

        for file in tax_fixtures:
            with open(f"{self.tax_fixture_dir}/{file}", "r") as tax_fixture:
                taxes = yaml.load(tax_fixture, Loader=yaml.SafeLoader)
                year = int(file.split(".")[0])
                self.tax_map[year] = [
                    TaxBracket(**bracket)
                    for bracket in taxes["brackets"][self.tax_filing_status]
                ]
