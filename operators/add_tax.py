from .base_operator import BaseOperator
from models import TaxBracket, IncomeProfile


class AddTax(BaseOperator):
    def __init__(self, tax: TaxBracket, *args):
        self.tax = tax
        super().__init__(*args)

    def apply(self, profile: IncomeProfile):
        profile.taxes.append(self.tax)
