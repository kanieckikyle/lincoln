from abc import ABCMeta
from datetime import datetime
import random
import string

from models import IncomeProfile


class BaseOperator(metaclass=ABCMeta):
    pk: int

    def __init__(self, execute_time: datetime):
        self.pk = self.generate_pk()
        self.execute_time = execute_time

    def apply(self, profile: IncomeProfile):
        pass

    def generate_pk(self) -> str:
        return "".join(random.choices(string.ascii_uppercase + string.digits, k=6))
