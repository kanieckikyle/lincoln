from .base_operator import BaseOperator
from models import IncomeProfile

from typing import Callable


class EditProfile(BaseOperator):
    def __init__(self, *args, callback: Callable[[IncomeProfile], None] = None):
        super().__init__(*args)
        self.callback = callback

    def apply(self, profile):
        if self.callback:
            self.callback(profile)
