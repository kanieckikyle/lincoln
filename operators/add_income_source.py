from .base_operator import BaseOperator
from models import IncomeSource, IncomeProfile


class AddIncomeSource(BaseOperator):
    def __init__(self, source: IncomeSource, *args):
        self.source = source
        super().__init__(*args)

    def apply(self, profile: IncomeProfile):
        profile.income_sources.append(self.source)
