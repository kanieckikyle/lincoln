from .base_operator import BaseOperator
from models import Expense, IncomeProfile


class AddExpense(BaseOperator):
    def __init__(self, expense: Expense, *args):
        self.expense = expense
        super().__init__(*args)

    def apply(self, profile: IncomeProfile):
        profile.expenses.append(self.expense)
