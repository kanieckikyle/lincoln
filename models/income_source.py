from pydantic import BaseModel
from enum import Enum
from .mixins import TickableModel


class IncomeSource(TickableModel):

    name: str

    amount: float
