from datetime import datetime, timedelta
from typing import ClassVar, List, Optional

from dateutil.rrule import rrule
from enums import FrequencyRule, get_rrule_from_freq_type
from pydantic import BaseModel, Field, PrivateAttr


def binary_search(arr, val, start, end):

    # we need to distinugish whether we
    # should insert before or after the
    # left boundary. imagine [0] is the last
    # step of the binary search and we need
    # to decide where to insert -1
    if start == end:
        if arr[start] > val:
            return start
        else:
            return start + 1

    # this occurs if we are moving
    # beyond left's boundary meaning
    # the left boundary is the least
    # position to find a number greater than val
    if start > end:
        return start

    mid = (start + end) // 2
    if arr[mid] < val:
        return binary_search(arr, val, mid + 1, end)
    elif arr[mid] > val:
        return binary_search(arr, val, start, mid - 1)
    else:
        return mid


class TickableModel(BaseModel):

    frequency: FrequencyRule = FrequencyRule.Yearly

    _timestamp: datetime = PrivateAttr(default_factory=datetime.now)

    _ticks: List[datetime] = PrivateAttr(default=[])

    _current_tick_index = PrivateAttr(default=0)

    _last_tick_metadata = PrivateAttr(default={})

    def build_ticks(self, start: datetime, end: datetime) -> None:
        self._timestamp = start
        kwargs = {"dtstart": start, "until": end}
        if self.frequency == FrequencyRule.Once:
            # As of version 2.5.0, the use of the keyword until in conjunction with
            # count is deprecated
            kwargs["count"] = 2
            del kwargs["until"]
        if self.frequency == FrequencyRule.Quarterly:
            kwargs["interval"] = 3
        if self.frequency == FrequencyRule.BiWeekly:
            kwargs["interval"] = 2

        self._ticks = list(rrule(get_rrule_from_freq_type(self.frequency), **kwargs))

    def tick(self, amount=None) -> int:
        if not amount:
            amount = timedelta(days=1)

        self._timestamp += amount
        old_index = self._current_tick_index

        next_tick = self.get_tick_at(self._current_tick_index + 1)
        while next_tick and next_tick < self._timestamp:
            self._current_tick_index += 1
            next_tick = self.get_tick_at(self._current_tick_index + 1)

        self.set_last_tick_metadata({})
        return self._current_tick_index - old_index

    def set_last_tick_metadata(self, metadata: dict):
        self._last_tick_metadata = metadata

    def get_current_tick(self) -> Optional[datetime]:
        return self.get_tick_at(self._current_tick_index)

    def get_tick_at(self, index: int) -> Optional[datetime]:
        if index < len(self._ticks) and index > -len(self._ticks):
            return self._ticks[index]
        return None

    def get_ticks_between(self, start: datetime, end: datetime) -> List[datetime]:
        num_ticks = len(self._ticks)
        start_index = binary_search(self._ticks, start, 0, num_ticks - 1)
        end_index = binary_search(self._ticks, end, start_index, num_ticks - 1)

        return self._ticks[start_index:end_index]
