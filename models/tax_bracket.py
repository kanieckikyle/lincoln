from pydantic import BaseModel
from enum import Enum


class TaxBracketCategory(Enum):
    Federal = 0
    State = 1
    Local = 2


class TaxBracket(BaseModel):
    name: str

    rate: int

    lower_threshold: float = 0.0

    upper_threshold: float = -1.0

    category: TaxBracketCategory = TaxBracketCategory.Federal

    def get_amount_owed(self, amount: float) -> float:
        if amount < self.lower_threshold:
            return 0.0

        if amount >= self.upper_threshold:
            return self.upper_threshold * (self.rate / 100)

        return (amount - self.lower_threshold) * (self.rate / 100)
