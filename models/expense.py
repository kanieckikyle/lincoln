from pydantic import BaseModel
from enum import Enum
from datetime import timedelta, datetime
from dateutil.rrule import rrule, MONTHLY, YEARLY, WEEKLY, DAILY
from math import ceil

from enums import FrequencyRule
from .mixins import TickableModel


class ExpenseType(Enum):
    Fixed = 0
    Percent = 1
    Hourly = 2


class Expense(TickableModel):
    name: str

    payment_amount: float = 0

    debt_amount: float = 0

    num_payments: int = 0

    deposit_account: str = ""

    type: ExpenseType = ExpenseType.Fixed

    pre_tax: bool = False

    category: int = 0

    def get_tick_cost(self, income: float = None):
        payment_cost = self.payment_amount
        if self.type == ExpenseType.Percent:
            assert (
                income is not None
            ), "You must provide an income when calculating payment cost for percentage expenses"
            payment_cost = income * (self.payment_amount / 100)

        return payment_cost
