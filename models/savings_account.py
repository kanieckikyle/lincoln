from pydantic import PrivateAttr, Field
from enum import Enum
from typing import List, Optional
from datetime import datetime
from dateutil.rrule import rrule

from .mixins import TickableModel
from enums import FrequencyRule, get_rrule_from_freq_type


class CompoundRate(Enum):
    Never = 0
    Yearly = 1
    Monthly = 2
    Weekly = 3
    Daily = 4


class SavingsAccount(TickableModel):

    name: str

    principal: float = 0.0

    total: float = 0.0

    interest: float = 0.0

    compound: Optional[TickableModel] = Field()

    def __post_init__(self):
        self.total = self.principal

    def build_ticks(self, start: datetime, end: datetime):
        super().build_ticks(start, end)
        if self.compound:
            self.compound.build_ticks(start, end)

    def apply_interest(self):
        self.total += self.principal * (self.interest / 100)

    def apply_compound(self):
        self.principal = self.total
