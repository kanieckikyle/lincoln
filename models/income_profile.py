from pydantic import BaseModel, Field, PrivateAttr
from datetime import datetime, timedelta
from typing import List, Optional
from math import ceil

from .mixins import TickableModel
from .tax_bracket import TaxBracket
from .expense import Expense
from .savings_account import SavingsAccount
from .income_source import IncomeSource


class IncomeProfile(TickableModel):

    income_sources: List[IncomeSource] = Field([])

    taxes: List[TaxBracket] = Field([])

    expenses: List[Expense] = Field([])

    savings: List[SavingsAccount] = Field([])

    default_savings: str = "savings"

    _default_savings_account: SavingsAccount = PrivateAttr()

    def __init__(self, **data):
        super().__init__(**data)
        if len(self.savings) == 0:
            self.savings.append(SavingsAccount(name=self.default_savings))
        self._default_savings_account = self.find_savings_account(self.default_savings)

    def tick(self, amount=None, save=True) -> int:
        super().tick(amount=amount)

        # Get the gross amount of money we made over this amount of time
        gross_income_over_tick = 0
        for income_source in self.income_sources:
            source_ticks = income_source.tick(amount=amount)
            gross_income_over_tick += income_source.amount * source_ticks

        # Get gross income adjusted for pre-tax expenses (deductibles)
        taxable_income_over_tick = gross_income_over_tick
        for expense in self.get_gross_expenses():
            expense_ticks = expense.tick(amount=amount)
            cost_over_tick = expense_ticks * expense.get_tick_cost(
                income=gross_income_over_tick
            )
            if save and expense.deposit_account:
                self.deposit_into_account(cost_over_tick, name=expense.deposit_account)
            taxable_income_over_tick -= cost_over_tick

        # Apply taxes to adjusted gross income
        net_income_over_tick = taxable_income_over_tick
        for tax in self.taxes:
            net_income_over_tick -= tax.get_amount_owed(taxable_income_over_tick)

        # Apply all of our net income expenses
        net_savings = net_income_over_tick
        for expense in self.get_net_expenses():
            expense_ticks = expense.tick(amount=amount)
            cost_over_tick = expense_ticks * expense.get_tick_cost(
                income=net_income_over_tick
            )
            if save and expense.deposit_account:
                self.deposit_into_account(cost_over_tick, name=expense.deposit_account)
            net_savings -= cost_over_tick

        if save:
            self.deposit_into_account(net_savings)

        # Apply interest to all of our accounts
        for acct in self.savings:
            interest_ticks = acct.tick(amount=amount)
            compound_ticks = 0
            step = interest_ticks
            # If this account has a frequency at which it compounds
            if acct.compound:
                # Calculate if the account should compound in our current tick
                compound_ticks = acct.compound.tick(amount=amount)
                # If it does, figure out the ratio at which the acct compunds
                # to that when it gets interest
                if compound_ticks > 0:
                    step = ceil(interest_ticks / (compound_ticks + 1))
                    

            # Skip the first compound, as 0 mod anything is always 0
            for i in range(1, interest_ticks + 1):
                # If we have any compound ticks and if we should compound now
                if compound_ticks and i % step == 0:
                    acct.apply_compound()
                
                # Apply interest to the account
                acct.apply_interest()

        self.set_last_tick_metadata(
            {
                "gross_income_over_tick": gross_income_over_tick,
                "taxable_income_over_tick": taxable_income_over_tick,
                "net_income_over_tick": net_income_over_tick,
                "net_savings": net_savings,
            }
        )

        return 0

    def deposit_into_account(self, amount: float, name: str = None):
        if acc := self.find_savings_account(name or self.default_savings):
            acc.total += amount
        else:
            self._default_savings_account.total += amount

    def find_savings_account(self, name: str) -> Optional[SavingsAccount]:
        for acc in self.savings:
            if acc.name == name:
                return acc
        else:
            return None
        
    def set_taxes(self, taxes: List[TaxBracket]):
        self.taxes = taxes

    def get_gross_expenses(self) -> List[Expense]:
        return [expense for expense in self.expenses if expense.pre_tax]

    def get_net_expenses(self) -> Expense:
        return [expense for expense in self.expenses if not expense.pre_tax]

    def print_accounts_table(self) -> None:
        for account in self.savings:
            print(f"{account.name}")
            print(f"\tTotal: {account.total}")
            print(f"\tPrincipal: {account.principal}")
            print(f"\tInterest Rate: {account.interest}")
